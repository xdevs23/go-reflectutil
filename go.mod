module gitlab.com/xdevs23/go-reflectutil

go 1.17

require (
	github.com/mitchellh/mapstructure v1.4.2
	github.com/pkg/errors v0.9.1
	github.com/stoewer/go-strcase v1.2.0
	github.com/toowoxx/go-structs v1.2.1
)
