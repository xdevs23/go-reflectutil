# Reflection utilities for Go

Various utilities to make the use of reflect easier.

These are utilities that you could have written yourself –
they are there to reduce boilerplate and is thus less error prone.
