package reflectutil
// SPDX-License-Identifier: Unlicense

import (
	"fmt"
	"strings"
	"testing"
)

type anotherTestStruct struct {
	u bool
}

func (a anotherTestStruct) BoolValue() bool {
	return a.u
}

type testStruct struct {
	x       int
	y       int
	Zyx     *testStruct
	Another anotherTestStruct
}

type testIFace interface {
	SetPos(x, y int)
	GetPos() (x, y int)
}

func (t *testStruct) SetPos(x, y int) {
	t.x = x
	t.y = y
}

func (t testStruct) GetPos() (x, y int) {
	return t.x, t.y
}

type StructForMapTest struct {
	SomeString string
	SomeInt int
}


func (t testStruct) TestMapToStruct(s StructForMapTest) (string, int) {
	return s.SomeString, s.SomeInt
}

func (t *testStruct) Reset() {
	t.x = 0
	t.y = 0
}

func (t *testStruct) TestSlice(strings []string) bool {
	return strings[1] == "A"
}

func (t testStruct) SomeMessages() (string, string, string, string, string) {
	return "It works", "really well", "🙃✅", ":)", "last one"
}

func TestCall(t *testing.T) {
	var result []interface{}
	var err error

	testObj := testStruct{}
	_, err = Call(&testObj, "SetPos", 1, 10)
	if err != nil {
		t.Fatal(err)
	}

	result, err = Call(testObj, "GetPos")
	if err != nil {
		t.Fatal(err)
	}
	if result[0] != 1 {
		t.Fatalf("expected first result (x) to be 1, was %d", result[0])
	}
	if result[1] != 10 {
		t.Fatalf("expected second result (y) to be 10, was %d", result[1])
	}

	_, err = Call(&testObj, "Reset")
	if err != nil {
		t.Fatal(err)
	}


	result, err = Call(testObj, "GetPos")
	if err != nil {
		t.Fatal(err)
	}
	if result[0] != 0 {
		t.Fatalf("expected first result (x) to be 0, was %d", result[0])
	}
	if result[1] != 0 {
		t.Fatalf("expected second result (y) to be 0, was %d", result[1])
	}

	_, err = Call(testObj, "NonExistentFunc")
	if err == nil {
		t.Fatal("Expected error, but no error occurred")
	}

	result, err = Call(testObj, "SomeMessages")
	if err != nil {
		t.Fatal(err)
	}
	resultStrings := make([]string, len(result))
	fmt.Println("Number of resultStrings returned:", len(resultStrings))

	for index, result := range result {
		resultStrings[index] = result.(string)
	}

	fmt.Println("Result:", strings.Join(resultStrings, " "))
}

func TestCallPath(t *testing.T) {
	testObj := testStruct{
		Zyx: &testStruct{
			Zyx: &testStruct{
				Zyx: &testStruct{
					Another: anotherTestStruct{
						u: true,
					},
				},
			},
		},
	}

	result, err := CallPath(testObj, "Zyx.Zyx.Zyx.Another.BoolValue")
	if err != nil {
		t.Fatal(err)
	}
	if result[0] != true {
		t.Fatal("expected true but got", result[0])
	}

	result, err = CallPath(testObj, "zyx.Zyx.zyx.another.boolValue")
	if err != nil {
		t.Fatal(err)
	}
	if result[0] != true {
		t.Fatal("expected true but got", result[0])
	}

	result, err = CallPath(testObj, "zyx.zyx.zyx.another.bool-value")
	if err != nil {
		t.Fatal(err)
	}
	if result[0] != true {
		t.Fatal("expected true but got", result[0])
	}

	result, err = CallPath(testObj, "zyx.Zyx.nonExistent")
	if err == nil {
		t.Fatal("expected error but didn't get one")
	}

	result, err = CallPath(testObj, "zyx.zyx.zyx.non-existent.bool-value")
	if err == nil {
		t.Fatal("expected error but didn't get one")
	}

	result, err = CallPath(testObj, "zyx.zyx.zyx.Another.nonExistent")
	if err == nil {
		t.Fatal("expected error but didn't get one")
	}

	result, err = CallPath(testObj, "zyx.zyx.SetPos", int16(15), int32(10))
	if err == nil {
		t.Fatal("expected error but didn't get one")
	}

	result, err = CallPath(testObj, "zyx.zyx.SetPos", 15, 10)
	if err != nil {
		t.Fatal(err)
	}

	result, err = CallPathAutoConvert(testObj, "zyx.zyx.GetPos")
	if err != nil {
		t.Fatal(err)
	}

	// result will be int
	if result[0] != 15 {
		t.Fatal("expected result[0] to be 15")
	}
	if result[1] != 10 {
		t.Fatal("expected result[1] to be 10")
	}
}


func TestCallPathAutoConvert(t *testing.T) {
	testObj := testStruct{
		Zyx: &testStruct{
			Zyx: &testStruct{
				Zyx: &testStruct{
					Another: anotherTestStruct{
						u: true,
					},
				},
			},
		},
	}

	result, err := CallPathAutoConvert(testObj, "Zyx.Zyx.Zyx.Another.BoolValue")
	if err != nil {
		t.Fatal(err)
	}
	if result[0] != true {
		t.Fatal("expected true but got", result[0])
	}

	result, err = CallPathAutoConvert(testObj, "zyx.Zyx.zyx.another.boolValue")
	if err != nil {
		t.Fatal(err)
	}
	if result[0] != true {
		t.Fatal("expected true but got", result[0])
	}

	result, err = CallPathAutoConvert(testObj, "zyx.zyx.zyx.another.bool-value")
	if err != nil {
		t.Fatal(err)
	}
	if result[0] != true {
		t.Fatal("expected true but got", result[0])
	}

	result, err = CallPathAutoConvert(testObj, "zyx.Zyx.nonExistent")
	if err == nil {
		t.Fatal("expected error but didn't get one")
	}

	result, err = CallPathAutoConvert(testObj, "zyx.zyx.zyx.non-existent.bool-value")
	if err == nil {
		t.Fatal("expected error but didn't get one")
	}

	result, err = CallPathAutoConvert(testObj, "zyx.zyx.zyx.Another.nonExistent")
	if err == nil {
		t.Fatal("expected error but didn't get one")
	}

	result, err = CallPathAutoConvert(testObj, "zyx.zyx.SetPos", int16(15), int32(10))
	if err != nil {
		t.Fatal(err)
	}

	result, err = CallPathAutoConvert(testObj, "zyx.zyx.GetPos")
	if err != nil {
		t.Fatal(err)
	}
	// result will be int
	if result[0] != 15 {
		t.Fatal("expected result[0] to be 15")
	}
	if result[1] != 10 {
		t.Fatal("expected result[1] to be 10")
	}

	result, err = CallPathAutoConvert(testObj, "zyx.zyx.zyx.TestSlice", "F", "O", "O")
	if err == nil {
		t.Fatal("expected error")
	}

	result, err = CallPathAutoConvert(testObj, "zyx.zyx.zyx.TestSlice", []string{"B", "A", "R"})
	if err != nil {
		t.Fatal(err)
	}
	if result[0] != true {
		t.Fatal("expected result[0] to be true")
	}

	result, err = CallPathAutoConvert(testObj, "zyx.zyx.zyx.TestMapToStruct", map[string]interface{}{
		"SomeString": "abc",
		"SomeInt": 300,
	})
	if err != nil {
		t.Fatal(err)
	}
	if result[0] != "abc" {
		t.Fatal("expected result[0] to be abc")
	}
	if result[1] != 300 {
		t.Fatal("expected result[1] to be 300")
	}

	result, err = CallPathAutoConvert(testObj, "zyx.zyx.zyx.TestMapToStruct", map[string]interface{}{
		"someString": "abcd",
		"someInt": 400,
	})
	if err != nil {
		t.Fatal(err)
	}
	if result[0] != "abcd" {
		t.Fatal("expected result[0] to be abcd")
	}
	if result[1] != 400 {
		t.Fatal("expected result[1] to be 400")
	}


	result, err = CallPathAutoConvert(testObj, "zyx.zyx.zyx.TestMapToStruct", map[string]interface{}{
		"aaaaa": "abcd",
		"bbbbb": 3003,
	})
	if err != nil {
		t.Fatal(err)
	}
	if result[0] != "" {
		t.Fatal("expected result[0] to be empty string")
	}
	if result[1] != 0 {
		t.Fatal("expected result[1] to be 0")
	}
}
