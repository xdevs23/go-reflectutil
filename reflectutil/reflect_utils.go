// Package reflectutil contains various utilities
// that are useful when working with reflection
package reflectutil
// SPDX-License-Identifier: Unlicense

import (
	"fmt"
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"github.com/toowoxx/go-structs"
	"math"
	"reflect"
	"runtime"
	"strings"
)

// GetModelName returns the struct name of `model`
// whereas model can be either an instance of a struct
// or reflect.Type. This function also does some additional
// checks and unwrapping to account for pointers, slices, etc.
func GetModelName(model interface{}) string {
	var ty reflect.Type
	switch model.(type) {
	case reflect.Type:
		ty = model.(reflect.Type)
	default:
		ty = reflect.TypeOf(model)
	}
	tyName := ty.Name()
	for ; len(tyName) == 0; ty = ty.Elem() {
		tyName = ty.Elem().Name()
	}
	if ty.Kind() == reflect.Slice || ty.Kind() == reflect.Array {
		tyName = "[]" + tyName
	}
	return tyName
}

// NewFromType creates a new instance of the given type
// and returns a pointer to the new instance
func NewFromType(modelType reflect.Type) interface{} {
	ptr := NewPtrValueFromType(modelType)
	obj := ptr.Interface()
	return obj
}

// NewPtrValueFromType creates a new instance of the given type
// and returns a reflect.Value of a pointer to the new instance
func NewPtrValueFromType(modelType reflect.Type) reflect.Value {
	return reflect.New(modelType)
}

// NewFromModel creates a new instance of the same type as `model`
// and returns a pointer to the new instance
func NewFromModel(model interface{}) interface{} {
	return NewFromType(reflect.TypeOf(model))
}

// SetSlice decodes all elements of `valueSlice` and puts them
// into a new slice which is then assigned to `fieldValue`.
// The expected `valueSlice` slice type is map[string]<anything>
func SetSlice(valueSlice []interface{}, fieldValue reflect.Value) {
	newSlice := reflect.MakeSlice(fieldValue.Type(), len(valueSlice), len(valueSlice))
	for i, val := range valueSlice {
		if reflect.TypeOf(val).Kind() == reflect.Map &&
			fieldValue.Type().Elem().Kind() == reflect.Struct {
			realVal := NewFromType(fieldValue.Type().Elem())
			mapstructure.Decode(val, realVal)
			if newSlice.Index(i).Kind() == reflect.Ptr {
				newSlice.Index(i).Set(reflect.ValueOf(realVal))
			} else {
				newSlice.Index(i).Set(reflect.ValueOf(realVal).Elem())
			}
		} else {
			newSlice.Index(i).Set(reflect.ValueOf(val))
		}
	}
	fieldValue.Set(newSlice)
}

// UnwrapType dereferences all pointers of `typ` and returns
// the actual dereferenced type
func UnwrapType(typ reflect.Type) (unwrapped reflect.Type) {
	unwrapped = typ
	for unwrapped.Kind() == reflect.Ptr {
		unwrapped = unwrapped.Elem()
	}
	return
}

// UnwrapValue dereferences all pointers of `val` and returns
// the actual dereferenced value
func UnwrapValue(val reflect.Value) (unwrapped reflect.Value) {
	unwrapped = val
	for unwrapped.Kind() == reflect.Ptr {
		unwrapped = unwrapped.Elem()
	}
	return
}

// WrapValue wraps `val` into a new pointer. This is equivalent to
// prefixing a non-generic (non-interface{}) variable with an ampersand
func WrapValue(val reflect.Value) reflect.Value {
	p := reflect.New(val.Type())
	p.Elem().Set(val)

	return p
}

// Wrap wraps `obj` into a new pointer. This is equivalent to
// prefixing a non-generic (non-interface{}) variable with an ampersand.
// Use this when you have an interface{} that needs to be a pointer
func Wrap(obj interface{}) interface{} {
	return WrapValue(reflect.ValueOf(obj)).Interface()
}

// Unwrap dereferences all pointers of `obj` and returns
// the actual dereferenced value
func Unwrap(obj interface{}) interface{} {
	val := reflect.ValueOf(obj)
	if val.Kind() != reflect.Ptr {
		return obj
	}
	return val.Elem().Interface()
}

// EnsureSinglePointer makes sure that `obj` is a pointer
// and points to a value. If `obj` is not a pointer, it will
// be wrapped into one. If what `obj` points to is also a pointer,
// it will be unwrapped until it points to a value.
func EnsureSinglePointer(obj interface{}) interface{} {
	return EnsureSinglePointerValue(reflect.ValueOf(obj)).Interface()
}

// EnsureSinglePointerValue makes sure that the kind of `val` is a pointer
// and points to a value. If the kind of `val` is not a pointer, it will
// be wrapped into one. If the element kind of `val` is also a pointer,
// it will be unwrapped until its element's kind is a value.
func EnsureSinglePointerValue(val reflect.Value) reflect.Value {
	if val.Kind() != reflect.Ptr {
		return WrapValue(val)
	}
	for val.Kind() == reflect.Ptr && val.Elem().Kind() == reflect.Ptr {
		val = val.Elem()
	}
	return val
}

// EnsureSinglePointerValue makes sure that the kind of `typ` is a pointer
// and points to a value. If the kind of `typ` is not a pointer, it will
// be wrapped into one. If the element kind of `typ` is also a pointer,
// it will be unwrapped until its element's kind is a value.
func EnsureSinglePointerType(typ reflect.Type) reflect.Type {
	if typ.Kind() != reflect.Ptr {
		return reflect.PtrTo(typ)
	}
	for typ.Kind() == reflect.Ptr && typ.Elem().Kind() == reflect.Ptr {
		typ = typ.Elem()
	}
	return typ
}

// AutoPointerNewFromType creates a new instance of `typ` and makes sure
// that the instance is a pointer and that it points to the new value (and
// not another pointer)
func AutoPointerNewFromType(typ reflect.Type) interface{} {
	return NewFromType(EnsureSinglePointerType(typ))
}

// AutoPointerNewFromType creates a new instance of the type of `val` and makes sure
// that the instance is a pointer and that it points to the new value (and
// not another pointer)
func AutoPointerNewFromValue(val reflect.Value) interface{} {
	return NewPtrValueFromType(EnsureSinglePointerType(val.Type()).Elem()).Interface()
}

// NewSlice creates a new slice of capacity 1 that has the same type as `model`
func NewSlice(model interface{}) interface{} {
	return NewSliceWithSize(model, 0, 1)
}

// NewSliceOfType creates a new slice of capacity 1 of type `typ`
func NewSliceOfType(typ reflect.Type) interface{} {
	return NewSliceOfTypeWithSize(typ, 0, 1)
}

// NewSliceOfTypeWithSize creates a new slice of length `len` and capacity `cap`
// of type `typ`. `cap` should be equal or greater than `len`.
func NewSliceOfTypeWithSize(typ reflect.Type, len int, cap int) interface{} {
	return UnwrapValue(reflect.MakeSlice(reflect.SliceOf(typ), len, cap)).Interface()
}

// NewSliceWithSize creates a new slice of length `len` and capacity `cap`
// that has the same type as `model`. `cap` should be equal or greater than `len`.
func NewSliceWithSize(model interface{}, len int, cap int) interface{} {
	return NewSliceOfTypeWithSize(EnsureSinglePointerType(reflect.TypeOf(model)).Elem(), len, cap)
}

// PurifyType dereferences all pointers, slices, maps, channels and arrays,
// effectively returning the pure type at the end of the chain. This
// means that the type `**[]*string` will be purified to `string` only.
func PurifyType(typ reflect.Type) reflect.Type {
	// According to reflect.Type.Elem() documentation
	for typ.Kind() == reflect.Array ||
		typ.Kind() == reflect.Chan ||
		typ.Kind() == reflect.Map ||
		typ.Kind() == reflect.Ptr ||
		typ.Kind() == reflect.Slice {

		typ = typ.Elem()
	}
	return typ
}

// PurifyType dereferences all pointers, slices, maps, channels and arrays,
// effectively returning the pure type at the end of the chain. This
// means that the type `**[]*string` will be purified to `string` only.
func PurifyToType(model interface{}) reflect.Type {
	return PurifyType(reflect.TypeOf(model))
}

// FuncNameOf returns the name of the function `fn`
func FuncNameOf(fn interface{}) string {
	v := reflect.ValueOf(fn)
	if rf := runtime.FuncForPC(v.Pointer()); rf != nil {
		return rf.Name()
	}
	return v.String()
}

// NameOf returns the name of whatever is passed as `anything`
func NameOf(anything interface{}) (str string) {
	t := reflect.TypeOf(anything)

	// In case this panics, trap it and return
	// a string representation of the reflect type
	defer func() {
		recover()
		str = t.String()
	}()

	switch t.Kind() {
	case reflect.Func:
		str = FuncNameOf(anything)
	default:
		str = PurifyType(t).String()
	}
	return
}

func Clone(obj interface{}) interface{} {
	return CloneValue(reflect.ValueOf(obj)).Interface()
}

func CloneValue(val reflect.Value) reflect.Value {
	return EnsureSinglePointerValue(UnwrapValue(val))
}

func GetFieldByName(obj interface{}, name string) interface{} {
	val := UnwrapValue(reflect.ValueOf(obj))
	typ := UnwrapType(reflect.TypeOf(obj))
	field, found := typ.FieldByName(name)
	if !found {
		return nil
	}
	return val.FieldByIndex(field.Index).Interface()
}

func SetFieldByName(obj interface{}, name string, fieldObj interface{}) bool {
	val := UnwrapValue(reflect.ValueOf(obj))
	typ := UnwrapType(reflect.TypeOf(obj))
	field, found := typ.FieldByName(name)
	if !found {
		return false
	}
	val.FieldByIndex(field.Index).Set(reflect.ValueOf(fieldObj))
	return true
}

func CopyValueToModel(modelType reflect.Type, modelValue reflect.Value, newModel interface{}) {
	newValue := UnwrapValue(reflect.ValueOf(newModel))
	if !modelValue.IsValid() {
		newValue.Set(reflect.Zero(newValue.Type()))
		return
	}
	if modelValue.Kind() == reflect.Ptr && (modelValue.IsNil() || modelValue.IsZero()) {
		newValue.Set(modelValue)
		return
	}
	newType := UnwrapType(reflect.TypeOf(newModel))
	for i := 0; i < newType.NumField(); i++ {
		f := newValue.Field(i)
		modF := newType.Field(i)
		origF, found := modelType.FieldByName(modF.Name)
		if found {
			f.Set(modelValue.FieldByIndex(origF.Index))
		}
	}
}

func Copy(from interface{}, to interface{}) interface{} {
	CopyValueToModel(UnwrapType(reflect.TypeOf(to)), UnwrapValue(reflect.ValueOf(from)), EnsureSinglePointer(to))
	return to
}

// ConvertInterfaceSliceToTyped converts items in a one-dimensional slice
// of type interface{} to items of the type that the toTyped slice is of
// and stores these items in the toTyped slice.
//
// Example scenario: convert []interface{} to []string
func ConvertInterfaceSliceToTyped(from []interface{}, toTyped interface{}) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = errors.Wrap(fmt.Errorf("panic: %v", r), "panic during ConvertInterfaceSliceToTyped")
		}
	}()

	origTyp := reflect.TypeOf(toTyped)
	if origTyp.Kind() != reflect.Ptr {
		return errors.New("toTyped is not a pointer to a slice")
	}
	typ := UnwrapType(origTyp)
	valTyped := reflect.ValueOf(toTyped)
	if typ.Kind() != reflect.Slice {
		switch valTyped.Elem().Interface().(type) {
		case []interface{}:
			typ = reflect.TypeOf([]interface{}{})
		default:
			return errors.New("toTyped does not point to a slice")
		}
	} else {
		typ = typ.Elem()
	}

	fromLen := len(from)
	valTyped.Elem().Set(reflect.ValueOf(NewSliceOfTypeWithSize(typ, fromLen, fromLen)))
	for index := range from {
		valTyped.Elem().Index(index).Set(reflect.ValueOf(from[index]).Convert(typ))
	}

	return
}

// ConvertInterfaceSliceToType converts items in a one-dimensional slice
// of type interface{} to items of the type specified by typ
// and stores these items in the to slice.
//
// Example scenario: convert []interface{} to []string
func ConvertInterfaceSliceToType(from []interface{}, to interface{}, typ reflect.Type) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = errors.Wrap(fmt.Errorf("panic: %v", r), "panic during ConvertInterfaceSliceToType")
		}
	}()

	origTyp := reflect.TypeOf(to)
	if origTyp.Kind() != reflect.Ptr {
		return errors.New("to is not a pointer to a slice")
	}
	valTyped := reflect.ValueOf(to)

	fromLen := len(from)
	newSliceVal := reflect.ValueOf(NewSliceOfTypeWithSize(typ.Elem(), fromLen, fromLen))
	for index := range from {
		elemVal := newSliceVal.Index(index)
		elemTyp := elemVal.Type()
		fromTyp := reflect.TypeOf(from[index])
		if typ.Elem().Kind() == reflect.Struct {
			elemPtr := EnsureSinglePointerValue(elemVal)
			err = ConvertSliceToStruct(from[index].([]interface{}), elemPtr.Interface())
			if err != nil {
				return
			}
			elemVal.Set(elemPtr.Elem())
		} else if elemVal.Kind() == reflect.Slice && fromTyp.Kind() == reflect.Slice {
			elemPtr := EnsureSinglePointerValue(elemVal)
			err = ConvertInterfaceSliceToType(from[index].([]interface{}), elemPtr.Interface(), elemTyp.Elem())
			if err != nil {
				return
			}
			elemVal.Set(elemPtr.Elem())
		} else {
			elemVal.Set(reflect.ValueOf(from[index]).Convert(typ))
		}
	}
	valTyped.Elem().Set(newSliceVal)

	return
}

func ConvertSliceToStruct(fromSlice interface{}, toStruct interface{}) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = errors.Wrap(fmt.Errorf("panic: %v", r), "panic during ConvertSliceToStruct")
		}
	}()
	wrappedStructTyp := reflect.TypeOf(toStruct)
	if wrappedStructTyp.Kind() != reflect.Ptr {
		return errors.New("toStruct is not a pointer to a struct")
	}
	structTyp := UnwrapType(wrappedStructTyp)

	if structTyp.Kind() != reflect.Struct {
		return errors.New("toStruct does not point to a struct ")
	}
	structVal := reflect.ValueOf(toStruct).Elem()

	sliceTyp := reflect.TypeOf(fromSlice)
	if sliceTyp.Kind() != reflect.Slice {
		return errors.New("fromSlice is not a slice")
	}
	sliceVal := reflect.ValueOf(fromSlice)

	sliceLen := sliceVal.Len()

	structNumField := structTyp.NumField()
	for i := 0; i < int(math.Min(float64(structNumField), float64(sliceLen))); i++ {
		fieldVal := structVal.Field(i)
		entryVal := sliceVal.Index(i)
		fieldTyp := fieldVal.Type()
		entryTyp := entryVal.Type()
		if entryTyp.AssignableTo(fieldTyp) {
			fieldVal.Set(entryVal)
		} else if entryTyp.ConvertibleTo(fieldTyp) {
			fieldVal.Set(entryVal.Convert(fieldTyp))
		} else {
			if entryTyp.Kind() == reflect.Map && fieldTyp.Kind() == reflect.Struct {
				entryPtr := EnsureSinglePointerValue(entryVal)
				fieldPtr := EnsureSinglePointerValue(fieldVal)
				if err := mapstructure.Decode(entryPtr.Interface(), fieldPtr.Interface()); err != nil {
					return errors.Wrapf(err, "could not decode %v into %v", entryTyp, fieldTyp)
				}
				fieldVal.Set(fieldPtr.Elem())
			} else if entryTyp.Kind() == reflect.Slice && fieldTyp.Kind() == reflect.Struct {
				fieldPtr := EnsureSinglePointerValue(fieldVal)
				if err := ConvertSliceToStruct(entryVal.Interface(), fieldPtr.Interface()); err != nil {
					return errors.Wrapf(err, "could not convert %v to %v", entryTyp, fieldTyp)
				}
				fieldVal.Set(fieldPtr.Elem())
			} else {
				entry := entryVal.Interface()
				switch fieldVal.Interface().(type) {
				case int, int8, int16, int32, int64:
					var n int64 = 0
					switch entry.(type) {
					case uint8:
						n = int64(entry.(uint8))
					case uint16:
						n = int64(entry.(uint16))
					case uint32:
						n = int64(entry.(uint32))
					case uint64:
						n = int64(entry.(uint64))
					case uint:
						n = int64(entry.(uint))
					case int8:
						n = int64(entry.(int8))
					case int16:
						n = int64(entry.(int16))
					case int32:
						n = int64(entry.(int32))
					case int64:
						n = entry.(int64)
					case int:
						n = int64(entry.(int))
					}
					fieldVal.SetInt(n)
				case string:
					fieldVal.SetString(entry.(string))
				case float32, float64:
					var f float64
					switch entry.(type) {
					case float32:
						f = float64(entry.(float32))
					case float64:
						f = entry.(float64)
					}
					fieldVal.SetFloat(f)
				case bool:
					fieldVal.SetBool(entry.(bool))
				case uint, uint8, uint16, uint32, uint64:
					var n uint64 = 0
					switch entry.(type) {
					case uint8:
						n = uint64(entry.(uint8))
					case uint16:
						n = uint64(entry.(uint16))
					case uint32:
						n = uint64(entry.(uint32))
					case uint64:
						n = entry.(uint64)
					case uint:
						n = uint64(entry.(uint))
					case int8:
						n = uint64(entry.(int8))
					case int16:
						n = uint64(entry.(int16))
					case int32:
						n = uint64(entry.(int32))
					case int64:
						n = uint64(entry.(int64))
					case int:
						n = uint64(entry.(int))
					}
					fieldVal.SetUint(n)
				case complex64:
					fieldVal.SetComplex(complex128(entry.(complex64)))
				case complex128:
					fieldVal.SetComplex(entry.(complex128))
				case []byte:
					fieldVal.SetBytes(entry.([]byte))
				case map[string]interface{}:
					if entryTyp.Kind() == reflect.Struct {
						// Convert the map to a struct
						newVal := EnsureSinglePointerValue(fieldVal)
						if err := mapstructure.Decode(entry, newVal); err != nil {
							return errors.Wrap(err, "Cannot decode map to struct")
						}
						fieldVal.Set(newVal.Elem())
					} else {
						return fmt.Errorf("cannot find a way to convert %v to %v", entryTyp, fieldTyp)
					}
				default:
					switch entry.(type) {
					case []interface{}:
						fieldPtr := EnsureSinglePointerValue(fieldVal)
						if err := ConvertSliceToStruct(entryVal.Interface(), fieldPtr.Interface()); err != nil {
							return errors.Wrapf(err, "could not convert %v to %v", entryTyp, fieldTyp)
						}
						fieldVal.Set(fieldPtr.Elem())
						goto afterNoConvert
					}
					goto noConvert
				}
			}
			goto afterNoConvert
		noConvert:
			return fmt.Errorf("cannot find a way to convert %v to %v", entryTyp, fieldTyp)
		afterNoConvert:
		}
	}

	return
}

func ToValueSlice(slice []interface{}) []reflect.Value {
	values := make([]reflect.Value, len(slice))
	for index := range slice {
		values[index] = reflect.ValueOf(slice[index])
	}
	return values
}

func FromValueSlice(values []reflect.Value) []interface{} {
	slice := make([]interface{}, len(values))
	for index := range slice {
		slice[index] = values[index].Interface()
	}
	return slice
}

// IsStruct returns true if i is a struct
func IsStruct(i interface{}) bool {
	return reflect.TypeOf(i).Kind() == reflect.Struct
}

// IsPointer returns true if i is a pointer
func IsPointer(i interface{}) bool {
	return reflect.TypeOf(i).Kind() == reflect.Ptr
}

// IsTypeStruct returns true if the type denoted by typ is of kind reflect.Struct
func IsTypeStruct(typ reflect.Type) bool {
	return typ.Kind() == reflect.Struct
}

// IsTypePointer returns true if the type denoted by typ is of kind reflect.Ptr
func IsTypePointer(typ reflect.Type) bool {
	return typ.Kind() == reflect.Ptr
}


// Extract looks for a field in st for which matcher returns true and writes
// the value into out. Error is returned if the field could not be found.
func Extract(st interface{}, matcher func (name string, value interface{}) bool, out interface{}) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = errors.Wrap(fmt.Errorf("panic: %v", r), "panic during Extract")
		}
	}()
	typ := UnwrapType(reflect.TypeOf(st))
	outTyp := reflect.TypeOf(out)
	outVal := reflect.ValueOf(out)
	if !IsTypeStruct(typ) {
		return errors.New("st is not a struct or a pointer to a struct")
	}
	if !IsTypePointer(outTyp) {
		return errors.New("out is not a pointer")
	}

	structMap := structs.Map(st)
	for name, field := range structMap {
		if matcher(name, field) {
			outVal.Elem().Set(reflect.ValueOf(field))
			return nil
		}
	}

	err = errors.New("could not find field in st")
	return
}

// ExtractByName looks for a field with name in st and writes
// the value into out. Error is returned if the field could not be found.
func ExtractByName(st interface{}, name string, out interface{}) error {
	return Extract(st, func(fName string, value interface{}) bool {
		return fName == name
	}, out)
}

// ExtractBySuffix looks for a field with suffix in st and writes
// the value into out. Error is returned if the field could not be found.
func ExtractBySuffix(st interface{}, suffix string, out interface{}) error {
	return Extract(st, func(name string, value interface{}) bool {
		return strings.HasSuffix(name, suffix)
	}, out)
}

// ExtractByPrefix looks for a field with prefix in st and writes
// the value into out. Error is returned if the field could not be found.
func ExtractByPrefix(st interface{}, prefix string, out interface{}) error {
	return Extract(st, func(name string, value interface{}) bool {
		return strings.HasPrefix(name, prefix)
	}, out)
}
