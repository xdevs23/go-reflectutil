package reflectutil

// SPDX-License-Identifier: Unlicense

import (
	"errors"
	"fmt"
	"github.com/mitchellh/mapstructure"
	"github.com/stoewer/go-strcase"
	"reflect"
	"strings"
)

// Call calls the specified function on the instance,
// passing the specified arguments and returning the
// return values as a slice
func Call(instance interface{}, funcName string, args ...interface{}) (result []interface{}, err error) {
	defer func() {
		if r := recover(); r != nil {
			result = nil
			err = errors.New(fmt.Sprintf(
				"panic trying to call %s in type %s: %v",
				funcName, reflect.TypeOf(instance).String(), r,
			))
		}
	}()
	val := EnsureSinglePointerValue(reflect.ValueOf(instance))
	typ := EnsureSinglePointerType(reflect.TypeOf(instance))
	methodType, exists := typ.MethodByName(funcName)
	if !exists {
		return nil, errors.New(fmt.Sprintf(
			"method with name %s does not exist in type %s",
			funcName, typ.String(),
		))
	}
	method := val.Method(methodType.Index)
	return FromValueSlice(method.Call(ToValueSlice(args))), nil
}

// CallPath traverses the specified struct as specified by a
// dot-separated path and calls the function whose name is
// the last part of the path, passing the specified arguments
// to it and returning the return values as a slice
// Important to know here is that the parts in the path are
// transformed to UpperCamelCase which allows you to use any
// casing convention in the path.
func CallPath(instance interface{}, path string, args ...interface{}) (result []interface{}, err error) {
	defer func() {
		if r := recover(); r != nil {
			result = nil
			err = errors.New(fmt.Sprintf(
				"panic trying to call %s in type %s: %v",
				path, reflect.TypeOf(instance).String(), r,
			))
		}
	}()
	val := EnsureSinglePointerValue(reflect.ValueOf(instance))
	typ := EnsureSinglePointerType(reflect.TypeOf(instance))

	parts := strings.Split(path, ".")
	if len(parts) == 0 {
		return nil, errors.New("empty path, cannot call function")
	}

	for _, part := range parts[:len(parts)-1] {
		uccPart := strcase.UpperCamelCase(part)
		field := val.Elem().FieldByName(uccPart)
		if !field.IsValid() {
			return nil, fmt.Errorf("path not found or invalid value at \"%s\" (%s)", part, uccPart)
		}
		val = EnsureSinglePointerValue(field)
		typ = EnsureSinglePointerType(field.Type())
	}

	funcName := strcase.UpperCamelCase(parts[len(parts)-1])
	methodType, exists := typ.MethodByName(funcName)
	if !exists {
		return nil, errors.New(fmt.Sprintf(
			"method with name %s (path %s) does not exist in type %s",
			funcName, path, typ.String(),
		))
	}
	method := val.Method(methodType.Index)
	return FromValueSlice(method.Call(ToValueSlice(args))), nil
}

// CallPathAutoConvert traverses the specified struct as
// specified by a dot-separated path and calls the function
// whose name is the last part of the path, passing the
// specified arguments (if possible, automatically converting
// to a compatible data type), to it and returning the return
// values as a slice.
// Important to know here is that the parts in the path are
// transformed to UpperCamelCase which allows you to use any
// casing convention in the path.
func CallPathAutoConvert(instance interface{}, path string, args ...interface{}) (result []interface{}, err error) {
	defer func() {
		if r := recover(); r != nil {
			result = nil
			err = errors.New(fmt.Sprintf(
				"panic trying to call %s in type %s: %v",
				path, reflect.TypeOf(instance).String(), r,
			))
		}
	}()
	val := EnsureSinglePointerValue(reflect.ValueOf(instance))
	typ := EnsureSinglePointerType(reflect.TypeOf(instance))

	parts := strings.Split(path, ".")
	if len(parts) == 0 {
		return nil, errors.New("empty path, cannot call function")
	}

	for _, part := range parts[:len(parts)-1] {
		uccPart := strcase.UpperCamelCase(part)
		field := val.Elem().FieldByName(uccPart)
		if !field.IsValid() {
			return nil, fmt.Errorf("path not found or invalid value at \"%s\" (%s)", part, uccPart)
		}
		val = EnsureSinglePointerValue(field)
		typ = EnsureSinglePointerType(field.Type())
	}

	funcName := strcase.UpperCamelCase(parts[len(parts)-1])
	methodDef, exists := typ.MethodByName(funcName)
	if !exists {
		return nil, errors.New(fmt.Sprintf(
			"method with name %s (path %s) does not exist in type %s",
			funcName, path, typ.String(),
		))
	}
	method := val.Method(methodDef.Index)

	methodType := method.Type()
	argCount := methodType.NumIn()

	if len(args) < argCount {
		return nil, fmt.Errorf("method expects %d arguments but only %d were provided", argCount, len(args))
	}
	for argIndex := 0; argIndex < argCount; argIndex++ {
		inType := methodType.In(argIndex)
		// convert args[argIndex] to inType if necessary (e. g. int16 -> int32)
		val := reflect.ValueOf(args[argIndex])
		itemVal := reflect.ValueOf(args).Index(argIndex)
		switch itemVal.Interface().(type) {
		case []interface{}:
			if inType.Kind() == reflect.Slice {
				newVal := EnsureSinglePointerValue(itemVal)
				err = ConvertInterfaceSliceToType(itemVal.Interface().([]interface{}), newVal.Interface(), inType)
				if err != nil {
					return
				}
				itemVal.Set(newVal.Elem())
			} else if inType.Kind() == reflect.Struct {
				newVal := EnsureSinglePointerValue(itemVal)
				err = ConvertSliceToStruct(itemVal.Interface().([]interface{}), newVal.Interface())
				if err != nil {
					return
				}
				itemVal.Set(newVal.Elem())
			}
		case map[string]interface{}:
			if inType.Kind() == reflect.Map {
				itemVal.Set(val.Convert(inType))
			} else if inType.Kind() == reflect.Struct {
				// Convert the map to a struct
				newVal := NewPtrValueFromType(inType)
				err = mapstructure.Decode(args[argIndex], newVal.Interface())
				if err != nil {
					return
				}
				itemVal.Set(newVal.Elem())
			}
		default:
			itemVal.Set(val.Convert(inType))
		}
	}

	return FromValueSlice(method.Call(ToValueSlice(args))), nil
}
