package reflectutil

import (
	"fmt"
	"log"
	"os"
	"testing"
)

func TestConvertInterfaceSliceToTyped(t *testing.T) {
	testSlice := []interface{}{
		"hi",
		"it's",
		"me",
	}

	var result []string

	if err := ConvertInterfaceSliceToTyped(testSlice, &result); err != nil {
		t.Fatal(err)
	}

	if result[0] != "hi" || result[1] != "it's" || result[2] != "me" {
		fmt.Println("Test slice:", testSlice)
		fmt.Println("Result:", result)
		t.Fatal("result does not match rest slice")
	}

	if err := ConvertInterfaceSliceToTyped(testSlice, result); err == nil {
		t.Fatal("expected error")
	}

	if err := ConvertInterfaceSliceToTyped(nil, &result); err != nil {
		t.Fatal(err)
	}

	if len(result) != 0 {
		fmt.Println("Result:", result)
		t.Fatal("expected result to be empty")
	}

	testSlice[1] = os.Args

	if err := ConvertInterfaceSliceToTyped(testSlice, &result); err == nil {
		log.Println("result:", result)
		t.Fatal("expected error")
	}
}

func TestConvertSliceToStruct(t *testing.T) {
	testSlice := []interface{}{
		1,
		"2",
		[]interface{}{
			3,
			"4",
		},
	}
	testStruct := struct {
		One int
		Two string
		S   struct{
			Three int
			Four  string
		}
	}{}

	if err := ConvertSliceToStruct(testSlice, &testStruct); err != nil {
		t.Fatal(err)
	}
	if testStruct.One != 1 {
		t.Fatal("expected One == 1, is", testStruct.One)
	}
	if testStruct.Two != "2" {
		t.Fatal("expected Two == \"2\", is", testStruct.Two)
	}
	if testStruct.S.Three != 3 {
		t.Fatal("expected S.Three == 3, is", testStruct.S.Three)
	}
	if testStruct.S.Four != "4" {
		t.Fatal("expected S.Four == \"4\", is", testStruct.S.Four)
	}

	if err := ConvertSliceToStruct(testSlice, testStruct); err == nil {
		t.Fatal("expected error")
	}

}

type extractStruct struct {
	One int
	Two string
}

func TestExtractBySuffix(t *testing.T) {
	testStruct := extractStruct{
		1, "2",
	}

	var one int
	var two string

	err := ExtractBySuffix(testStruct, "wo", &two)
	if err != nil {
		t.Fatal(err)
	}
	if two != "2" {
		t.Fatalf("expected two to be \"2\", got %s instead", two)
	}

	err = ExtractBySuffix(&testStruct, "ne", &one)
	if err != nil {
		t.Fatal(err)
	}
	if one != 1 {
		t.Fatalf("expected one to be 1, got %d instead", one)
	}

	err = ExtractBySuffix(testStruct, "ree", &one)
	if err == nil {
		t.Fatal("expected error")
	}

	err = ExtractBySuffix(nil, "", nil)
	if err == nil {
		t.Fatal("expected error")
	}

	err = ExtractBySuffix(&one, "aa", &testStruct)
	if err == nil {
		t.Fatal("expected error")
	}

	var testStructDeep ****extractStruct
	testStruct1 := &testStruct
	testStruct2 := &testStruct1
	testStruct3 := &testStruct2
	testStructDeep = &testStruct3

	one = -1
	err = ExtractBySuffix(&testStructDeep, "ne", &one)
	if err != nil {
		t.Fatal(err)
	}
	if one != 1 {
		t.Fatalf("expected one to be 1, got %d instead", one)
	}

}